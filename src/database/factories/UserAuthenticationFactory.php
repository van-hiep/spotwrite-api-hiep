<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Entities\Models\User;
use App\Entities\Models\UserAuthentication;
use Faker\Generator as Faker;

$factory->define(UserAuthentication::class, function (Faker $faker) {
    return [
        'user_id' => function () {
            return factory(User::class)->create()->id;
        },
        'email' => $faker->unique()->safeEmail,
        'password' =>  $faker->password,
    ];
});
