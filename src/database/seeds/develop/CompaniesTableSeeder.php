<?php

namespace develop;

use Carbon\Carbon;
use DB;
use Illuminate\Database\Seeder;
use Str;

/**
 * Class CompaniesTableSeeder
 * @package develop
 */
class CompaniesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('companies')->insert([
            [
                'id' => 1,
                'uuid' => Str::uuid()->toString(),
                'name' => '株式会社01',
                'address' => '沖縄県那覇市おもろまち４丁目４−９',
                'tel' => '000-1111-2222',
                'size' => '100人規模',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ],
        ]);
    }
}
