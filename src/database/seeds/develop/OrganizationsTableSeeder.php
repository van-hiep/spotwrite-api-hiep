<?php

namespace develop;

use Carbon\Carbon;
use DB;
use Illuminate\Database\Seeder;
use Str;

/**
 * Class OrganizationsTableSeeder
 * @package develop
 */
class OrganizationsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->insertOrganizationCategories();
        $this->insertOrganizations();
        $this->insertOrganizationClosure();
        $this->insertOrganizationWorkflows();
        $this->insertOrganizationCareers();
        $this->insertOrganizationLeaders();
        $this->insertOrganizationUsers();
    }

    /**
     *
     */
    private function insertOrganizationCategories()
    {
        DB::table('organization_categories')->insert([
            [
                'id' => 1,
                'uuid' => Str::uuid()->toString(),
                'company_id' => 1,
                'name' => 'カテゴリ01',
                'description' => 'これはカテゴリ01です',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ],
        ]);
    }

    /**
     *
     */
    private function insertOrganizations()
    {

        DB::table('organizations')->insert([
            [
                'id' => 1,
                'uuid' => Str::uuid()->toString(),
                'company_id' => 1,
                'category_id' => 1,
                'name' => '組織1',
                'description' => '組織1の説明文です',
                'status' => 'public',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ],
            [
                'id' => 2,
                'uuid' => Str::uuid()->toString(),
                'company_id' => 1,
                'category_id' => 1,
                'name' => '組織1-1',
                'description' => '組織1-1の説明文です',
                'status' => 'public',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ],
            [
                'id' => 3,
                'uuid' => Str::uuid()->toString(),
                'company_id' => 1,
                'category_id' => 1,
                'name' => '組織1-2',
                'description' => '組織1-2の説明文です',
                'status' => 'public',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ],
            [
                'id' => 4,
                'uuid' => Str::uuid()->toString(),
                'company_id' => 1,
                'category_id' => 1,
                'name' => '組織1-1-1',
                'description' => '組織1-1-1の説明文です',
                'status' => 'public',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ],
        ]);
    }

    /**
     *
     */
    private function insertOrganizationWorkflows()
    {
        DB::table('organization_workflows')->insert([
            [
                'id' => 1,
                'uuid' => Str::uuid()->toString(),
                'organization_id' => 1,
                'starting_time' => '09:00',
                'title' => '仕事01',
                'content' => 'これは仕事01です',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ],
            [
                'id' => 2,
                'uuid' => Str::uuid()->toString(),
                'organization_id' => 2,
                'starting_time' => '09:00',
                'title' => '仕事01',
                'content' => 'これは仕事01です',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ],
            [
                'id' => 3,
                'uuid' => Str::uuid()->toString(),
                'organization_id' => 3,
                'starting_time' => '09:00',
                'title' => '仕事01',
                'content' => 'これは仕事01です',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ],
            [
                'id' => 4,
                'uuid' => Str::uuid()->toString(),
                'organization_id' => 4,
                'starting_time' => '09:00',
                'title' => '仕事01',
                'content' => 'これは仕事01です',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ],
            [
                'id' => 5,
                'uuid' => Str::uuid()->toString(),
                'organization_id' => 4,
                'starting_time' => '10:00',
                'title' => '仕事02',
                'content' => 'これは仕事02です',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ],
            [
                'id' => 6,
                'uuid' => Str::uuid()->toString(),
                'organization_id' => 4,
                'starting_time' => '11:00',
                'title' => '仕事03',
                'content' => 'これは仕事03です',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ],
        ]);
    }

    /**
     *
     */
    private function insertOrganizationCareers()
    {
        DB::table('organization_careers')->insert([
            [
                'id' => 1,
                'uuid' => Str::uuid()->toString(),
                'organization_id' => 1,
                'working_years' => 1,
                'title' => 'キャリア01',
                'content' => 'これはキャリア01です',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ],
            [
                'id' => 2,
                'uuid' => Str::uuid()->toString(),
                'organization_id' => 2,
                'working_years' => 1,
                'title' => 'キャリア01',
                'content' => 'これはキャリア01です',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ],
            [
                'id' => 3,
                'uuid' => Str::uuid()->toString(),
                'organization_id' => 3,
                'working_years' => 1,
                'title' => 'キャリア01',
                'content' => 'これはキャリア01です',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ],
            [
                'id' => 4,
                'uuid' => Str::uuid()->toString(),
                'organization_id' => 4,
                'working_years' => 1,
                'title' => 'キャリア01',
                'content' => 'これはキャリア01です',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ],
            [
                'id' => 5,
                'uuid' => Str::uuid()->toString(),
                'organization_id' => 4,
                'working_years' => 2,
                'title' => 'キャリア02',
                'content' => 'これはキャリア02です',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ],
            [
                'id' => 6,
                'uuid' => Str::uuid()->toString(),
                'organization_id' => 4,
                'working_years' => 3,
                'title' => 'キャリア03',
                'content' => 'これはキャリア03です',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ],
        ]);
    }

    /**
     *
     */
    private function insertOrganizationClosure()
    {
        DB::table('organization_closure')->insert([
            [
                'id' => 1,
                'uuid' => Str::uuid()->toString(),
                'parent_id' => 1,
                'child_id' => 1,
                'depth' => 0,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ],
            [
                'id' => 2,
                'uuid' => Str::uuid()->toString(),
                'parent_id' => 1,
                'child_id' => 2,
                'depth' => 1,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ],
            [
                'id' => 3,
                'uuid' => Str::uuid()->toString(),
                'parent_id' => 1,
                'child_id' => 3,
                'depth' => 1,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ],
            [
                'id' => 4,
                'uuid' => Str::uuid()->toString(),
                'parent_id' => 1,
                'child_id' => 4,
                'depth' => 2,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ],
            [
                'id' => 5,
                'uuid' => Str::uuid()->toString(),
                'parent_id' => 2,
                'child_id' => 2,
                'depth' => 0,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ],
            [
                'id' => 6,
                'uuid' => Str::uuid()->toString(),
                'parent_id' => 2,
                'child_id' => 4,
                'depth' => 1,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ],
            [
                'id' => 7,
                'uuid' => Str::uuid()->toString(),
                'parent_id' => 3,
                'child_id' => 3,
                'depth' => 1,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ],
            [
                'id' => 8,
                'uuid' => Str::uuid()->toString(),
                'parent_id' => 4,
                'child_id' => 4,
                'depth' => 1,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ],
        ]);
    }

    /**
     *
     */
    private function insertOrganizationLeaders()
    {
        DB::table('organization_leaders')->insert([
            [
                'id' => 1,
                'uuid' => Str::uuid()->toString(),
                'organization_id' => 1,
                'user_id' => 1,
                'message' => 'これはリーダのメッセージ01です',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ],
            [
                'id' => 2,
                'uuid' => Str::uuid()->toString(),
                'organization_id' => 2,
                'user_id' => 2,
                'message' => 'これはリーダのメッセージ02です',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ],
            [
                'id' => 3,
                'uuid' => Str::uuid()->toString(),
                'organization_id' => 3,
                'user_id' => 3,
                'message' => 'これはリーダのメッセージ03です',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ],
        ]);
    }

    /**
     *
     */
    private function insertOrganizationUsers()
    {
        DB::table('organization_users')->insert([
            [
                'id' => 1,
                'uuid' => Str::uuid()->toString(),
                'organization_id' => 1,
                'user_id' => 1,
                'priority' => 'main',
                'employment_position_id' => 1,
                'employment_status_id' => 1,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ],
            [
                'id' => 2,
                'uuid' => Str::uuid()->toString(),
                'organization_id' => 2,
                'user_id' => 2,
                'priority' => 'main',
                'employment_position_id' => 1,
                'employment_status_id' => 1,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ],
            [
                'id' => 3,
                'uuid' => Str::uuid()->toString(),
                'organization_id' => 3,
                'user_id' => 3,
                'priority' => 'main',
                'employment_position_id' => 1,
                'employment_status_id' => 1,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ],
        ]);
    }
}
