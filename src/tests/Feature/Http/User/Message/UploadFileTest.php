<?php

namespace Tests\Feature\Http\User\Message;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Http\Response;
use Tests\TestCase;
use App\Entities\Models\User;
use Illuminate\Support\Facades\Storage;
use Illuminate\Http\UploadedFile;

/**
 * Class UploadImage
 * @package Tests\Feature\Http\User\Message
 */
class UploadFileTest extends TestCase
{
    /**
     * @noinspection NonAsciiCharacters
     * @test
     */
    public function 添付ファイルの保存に成功する(): void
    {
        Storage::fake(config('filesystems.default'));

        $user = factory(User::class)->create();
        $this->authorizeWithJwt($user);

        $request = [
            'file' => UploadedFile::fake()->create('sample.xls', 500, 'xls'),
        ];

        $response = $this->postJson(route('message.store.uploadFile'), $request);

        $response->assertStatus(Response::HTTP_OK);
        $response->assertJsonStructure(['file_url']);

        Storage::disk(config('filesystems.default'))
            ->assertExists(explode("/storage", $response->json()['file_url'])[1]);
    }
}
