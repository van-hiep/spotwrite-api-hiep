<?php

namespace Tests\Feature\Http\Company;

use App\Entities\Models\AccountHolder;
use App\Entities\Models\Company;
use App\Entities\Models\Organization;
use App\Entities\Models\OrganizationCategory;
use App\Entities\Models\OrganizationClosure;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Http\Response;
use Tests\TestCase;

/**
 * Class OrganizationsTest
 * @package Tests\Feature\Http\Company
 */
class OrganizationsTest extends TestCase
{
    use RefreshDatabase;

    /**
     * @noinspection NonAsciiCharacters
     * @test
     */
    public function 組織の一覧が返る(): void
    {
        $company = factory(Company::class)->create();
        $accountHolder = factory(AccountHolder::class)->create([
            'company_id' => $company->id,
        ]);
        $category = OrganizationCategory::create([
            'company_id' => $company->id,
            'name' => 'カテゴリ01',
            'description' => 'これはカテゴリ01です',
        ]);
        $organization1 = Organization::create([
            'company_id' => $company->id,
            'category_id' => $category->id,
            'name' => '組織01',
            'description' => '説明文です',
            'status' => 'public',
        ]);
        $organization2 = Organization::create([
            'company_id' => $company->id,
            'category_id' => $category->id,
            'name' => '組織02',
            'description' => '説明文です',
            'status' => 'public',
        ]);
        OrganizationClosure::create([
            'parent_id' => $organization1->id,
            'child_id' => $organization1->id,
            'depth' => 0,
        ]);
        OrganizationClosure::create([
            'parent_id' => $organization2->id,
            'child_id' => $organization2->id,
            'depth' => 0,
        ]);

        $this->authorizeWithJwt($accountHolder);
        $response = $this->getJson(route('company.organizations.index', ['company' => $company->uuid]));
        $response->assertStatus(Response::HTTP_OK);
        $response->assertJsonCount(2, 'organizations');
        $response->assertJsonStructure([
            'organizations' => [
                [
                    'uuid',
                    'category_uuid',
                    'category',
                    'name',
                    'description',
                    'status',
                    'file_uuid',
                ]
            ],
        ]);
        $response->assertJson([
            'organizations' => [
                [
                    'uuid' => $organization1->uuid,
                    'category_uuid' => $organization1->category->uuid,
                    'category' => $organization1->category->name,
                    'name' => $organization1->name,
                    'description' => $organization1->description,
                    'status' => $organization1->status,
                    'file_uuid' => null,
                ],
                [
                    'uuid' => $organization2->uuid,
                    'category_uuid' => $organization2->category->uuid,
                    'category' => $organization2->category->name,
                    'name' => $organization2->name,
                    'description' => $organization2->description,
                    'status' => $organization2->status,
                    'file_uuid' => null,
                ],
            ],
        ]);
    }

    /**
     * @noinspection NonAsciiCharacters
     * @test
     */
    public function 組織の詳細が返る(): void
    {
        $company = factory(Company::class)->create();
        $accountHolder = factory(AccountHolder::class)->create([
            'company_id' => $company->id,
        ]);
        $category = OrganizationCategory::create([
            'company_id' => $company->id,
            'name' => 'カテゴリ01',
            'description' => 'これはカテゴリ01です',
        ]);
        $organization = Organization::create([
            'company_id' => $company->id,
            'category_id' => $category->id,
            'name' => '組織01',
            'description' => '説明文です',
            'status' => 'public',
        ]);
        OrganizationClosure::create([
            'parent_id' => $organization->id,
            'child_id' => $organization->id,
            'depth' => 0,
        ]);

        $this->authorizeWithJwt($accountHolder);
        $response = $this->getJson(
            route('company.organizations.show', [
                'company' => $company->uuid,
                'organization' => $organization->uuid,
            ])
        );
        $response->assertStatus(Response::HTTP_OK);
        $response->assertJsonStructure([
            'uuid',
            'category_uuid',
            'category',
            'name',
            'description',
            'status',
            'file_uuid',
        ]);
        $response->assertJson([
            'uuid' => $organization->uuid,
            'category_uuid' => $organization->category->uuid,
            'category' => $organization->category->name,
            'name' => $organization->name,
            'description' => $organization->description,
            'status' => $organization->status,
            'file_uuid' => null,
        ]);
    }

    /**
     * @noinspection NonAsciiCharacters
     * @test
     */
    public function 組織を作成すると、作成した組織が返る(): void
    {
        $company = factory(Company::class)->create();
        $accountHolder = factory(AccountHolder::class)->create([
            'company_id' => $company->id,
        ]);
        $category = OrganizationCategory::create([
            'company_id' => $company->id,
            'name' => 'カテゴリ01',
            'description' => 'これはカテゴリ01です',
        ]);

        $this->authorizeWithJwt($accountHolder);
        $request = [
            'category_uuid' => $category->uuid,
            'name' => '人事部',
            'description' => '人を管理する部署だよ',
            'status' => 'public',
            'file_uuid' => null,
        ];
        $response = $this->postJson(
            route('company.organizations.create', ['company' => $company->uuid]),
            $request
        );
        $newOrganization = Organization::ofUuid($response->json('uuid'))->first();
        $response->assertStatus(Response::HTTP_OK);
        $response->assertJsonStructure([
            'uuid',
            'category_uuid',
            'category',
            'name',
            'description',
            'status',
            'file_uuid',
        ]);
        $response->assertJson([
            'uuid' => $newOrganization->uuid,
            'category_uuid' => $category->uuid,
            'category' => $category->name,
            'name' => $request['name'],
            'description' => $request['description'],
            'status' => $request['status'],
            'file_uuid' => null,
        ]);
        $this->assertDatabaseHas('organizations', [
            'uuid' => $newOrganization->uuid,
            'company_id' => $company->id,
            'category_id' => $category->id,
            'name' => $request['name'],
            'description' => $request['description'],
            'status' => $request['status'],
        ]);
        $this->assertDatabaseHas('organization_closure', [
            'parent_id' => $newOrganization->id,
            'child_id' => $newOrganization->id,
            'depth' => 0,
        ]);
    }

    /**
     * @noinspection NonAsciiCharacters
     * @test
     */
    public function 親組織を指定して組織を作成すると、作成した組織が返る(): void
    {
        $company = factory(Company::class)->create();
        $accountHolder = factory(AccountHolder::class)->create([
            'company_id' => $company->id,
        ]);
        $category = OrganizationCategory::create([
            'company_id' => $company->id,
            'name' => 'カテゴリ01',
            'description' => 'これはカテゴリ01です',
        ]);
        $organization = Organization::create([
            'company_id' => $company->id,
            'category_id' => $category->id,
            'name' => '組織01',
            'description' => '説明文です',
            'status' => 'public',
        ]);
        OrganizationClosure::create([
            'parent_id' => $organization->id,
            'child_id' => $organization->id,
            'depth' => 0,
        ]);

        $this->authorizeWithJwt($accountHolder);
        $request = [
            'category_uuid' => $category->uuid,
            'name' => '人事部',
            'description' => '人を管理する部署だよ',
            'status' => 'public',
            'file_uuid' => null,
        ];
        $response = $this->postJson(
            route('company.organizations.create', [
                'company' => $company->uuid,
                'organization' => $organization->uuid,
            ]),
            $request
        );
        $newOrganization = Organization::ofUuid($response->json('uuid'))->first();
        $response->assertStatus(Response::HTTP_OK);
        $response->assertJsonStructure([
            'uuid',
            'category_uuid',
            'category',
            'name',
            'description',
            'status',
            'file_uuid',
        ]);
        $response->assertJson([
            'uuid' => $newOrganization->uuid,
            'category_uuid' => $category->uuid,
            'category' => $category->name,
            'name' => $request['name'],
            'description' => $request['description'],
            'status' => $request['status'],
            'file_uuid' => null,
        ]);
        $this->assertDatabaseHas('organizations', [
            'uuid' => $newOrganization->uuid,
            'company_id' => $company->id,
            'category_id' => $category->id,
            'name' => $request['name'],
            'description' => $request['description'],
            'status' => $request['status'],
        ]);
        $this->assertDatabaseHas('organization_closure', [
            'parent_id' => $newOrganization->id,
            'child_id' => $newOrganization->id,
            'depth' => 0,
        ]);
        $this->assertDatabaseHas('organization_closure', [
            'parent_id' => $organization->id,
            'child_id' => $newOrganization->id,
            'depth' => 1,
        ]);
    }

    /**
     * @noinspection NonAsciiCharacters
     * @test
     */
    public function ログインしていない場合、401が返る(): void
    {
        $company = factory(Company::class)->create();
        $accountHolder = factory(AccountHolder::class)->create([
            'company_id' => $company->id,
        ]);

        $response = $this->getJson(route('company.organizations.index', ['company' => $company->uuid]));
        $response->assertStatus(Response::HTTP_UNAUTHORIZED);
    }
}
