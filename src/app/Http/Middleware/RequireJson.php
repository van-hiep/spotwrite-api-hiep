<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

/**
 * リクエストヘッダに Accept: application/json を追加する
 *
 * Class RequireJson
 * @package App\Http\Middleware
 */
class RequireJson
{
    /**
     * @param Request $request
     * @param Closure $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        $request->headers->set('Accept', 'application/json');
        $response = $next($request);

        return $response;
    }
}
