<?php

namespace App\Http\Responses\Company\Organizations;

use App\Http\Resources\OrganizationResource;
use Illuminate\Support\Collection;

/**
 * Class IndexResponse
 * @package App\Http\Responses\Company\Organizations
 */
class IndexResponse
{
    /**
     * @param Collection $organizations
     * @return array
     */
    public static function body(Collection $organizations): array
    {
        return ['organizations' => OrganizationResource::collection($organizations)];
    }
}
