<?php

namespace App\Http\Requests\User\Message;

use Illuminate\Foundation\Http\FormRequest;

class RoomRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        // TODO: 指定されたユーザがチャットのルームに入っても大丈夫なのかバリデーションとる必要がでてきそう
        return [
            'user_uuids' => ['required', 'array'],
            'user_uuids.*' => ['exists:users,uuid']
        ];
    }
}
