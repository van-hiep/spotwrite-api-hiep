<?php

namespace App\Entities\Models;

use App\Entities\Traits\UuidTrait;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

/**
 * Class OrganizationClosure
 * @package App\Entities\Models
 */
class OrganizationClosure extends Model
{
    use UuidTrait;

    /**
     * @var string
     */
    protected $table = 'organization_closure';

    /**
     * @var string[]
     */
    protected $fillable = [
        'parent_id',
        'child_id',
        'depth',
    ];

    /**
     * @param Builder $query
     * @param int $childId
     * @return Builder
     */
    public function scopeOfChildId(Builder $query, int $childId): Builder
    {
        return $query->where('child_id', $childId);
    }
}
