<?php

namespace App\Entities\Models;

use App\Entities\Traits\UuidTrait;
use Illuminate\Database\Eloquent\Model;

/**
 * Class OrganizationImage
 * @package App\Entities\Models
 */
class OrganizationImage extends Model
{
    use UuidTrait;

    /**
     * @var string[]
     */
    protected $fillable = [
        'organization_id',
        'company_file_id',
    ];
}
